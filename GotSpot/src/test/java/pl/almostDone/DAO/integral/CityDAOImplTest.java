package pl.almostDone.DAO.integral;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javax.persistence.TypedQuery;

import org.junit.AfterClass;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

import pl.almostDone.DAO.impl.CityDAOImpl;
import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.entities.City;

import java.util.ArrayList;
import java.util.List;

public class CityDAOImplTest {

	static EntityManagerFactory entityManagerFactory;
	static EntityManager em;
	static CityDAOImpl cityDAOImpl;
	static ArrayList<City> listOfCities;

	@BeforeClass
	public static void setupBeforeClass() {
		entityManagerFactory = Persistence.createEntityManagerFactory("GotSpotPUTest");
		em = entityManagerFactory.createEntityManager();
		City city1 = new City("Sierpc", "");
		City city2 = new City("Warszawa", "Bemowo");
		City city3 = new City("Wieden", "");
		City city4 = new City("Warka", "");
		listOfCities = new ArrayList<City>();
		listOfCities.add(city1);
		listOfCities.add(city2);
		listOfCities.add(city3);
		listOfCities.add(city4);
		cityDAOImpl = new CityDAOImpl();
		cityDAOImpl.setEntityManager(em);

		em.getTransaction().begin();
		for (City city : listOfCities) {
			em.persist(city);
		}
		em.getTransaction().commit();

	}

	@Test
	public void testCityExist_City_Not_Exist_In_DB() throws CityExistsInDBException {

		City city = new City("Test", "Test");

		Long previous = cityDAOImpl.getCitiesCount();
		assertEquals(Long.valueOf((long) listOfCities.size()), previous);

		City addedCity = cityDAOImpl.addCity(city);
		assertNotNull(addedCity.getId());

		Long now = cityDAOImpl.getCitiesCount();

		assertEquals((Long) (previous + 1), now);
	}

	@Test(expected = CityExistsInDBException.class)
	public void testCityExist_City__Exist_In_DB() throws CityExistsInDBException {
		TypedQuery<Long> query = em.createQuery("SELECT COUNT(c) FROM City c", Long.class);
		Long previous = query.getSingleResult();

		City city = new City("Warszawa", "Bemowo");

		City foundCity = cityDAOImpl.addCity(city);
		assertNotNull(foundCity);

		Long now = query.getSingleResult();
		assertEquals(previous, now);
		System.out.println("Test przeszedł");
	}

	@Test
	public void test_getListMatchedCities() {

		String substring1 = "W";
		String substring2 = "War";
		String substring3 = "CHUJ";
		String substring4 = "Sierp";

		List<City> list;

		list = cityDAOImpl.getListmatchedCities(substring1);

		assertNotNull(list);
		assertThat(list).extracting("city").contains("Warka", "Wieden", "Warszawa");

		list = cityDAOImpl.getListmatchedCities(substring2);

		assertNotNull(list);
		assertThat(list).extracting("city").contains("Warka", "Warszawa");

		list = cityDAOImpl.getListmatchedCities(substring3);

		assertNotNull(list);
		assertThat(list.isEmpty());

		list = cityDAOImpl.getListmatchedCities(substring4);

		assertNotNull(list);
		assertThat(list).extracting("city").contains("Sierpc");

	}

	@AfterClass
	public static void teardown() {
		em.close();
		entityManagerFactory.close();
	}

}
