package pl.almostDone.Exceptions;

import pl.almostDone.entities.City;

public class CityExistsInDBException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private City city;
	
	

	public City getCity() {
		return city;
	}



	public void setCity(City city) {
		this.city = city;
	}



	public CityExistsInDBException(String message) {
		super(message);
	}
	
	public CityExistsInDBException(String message, City city) {
		super(message);
		this.city = city;
	}
}
