package pl.almostDone.Exceptions;

import pl.almostDone.entities.User;

public class UserExistsInDBException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7232007437467504558L;

	private User user;
	
	public UserExistsInDBException(String message, User user) {
		super(message);
		this.user = user;
	}
	
}
