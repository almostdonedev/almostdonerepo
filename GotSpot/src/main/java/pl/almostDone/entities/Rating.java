package pl.almostDone.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Rating {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rating_id")
	private Long id;
	private SingleRating rating;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	private enum SingleRating {
	ZERO(0), ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);
		
		private Integer rating;
		
		private SingleRating(Integer singleRating) {
			this.rating = singleRating;
		}
		
	}
	
}
