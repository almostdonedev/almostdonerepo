package pl.almostDone.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NamedQueries;

@Entity
@Table(name = "Cities", uniqueConstraints = @UniqueConstraint(columnNames = { "city", "district" }))
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id", nullable = false)
	private Long id;
	@Column(name = "city", nullable = false)
	private String city;
	@Column(name = "district", nullable = true)
	private String district;
	
	
	public Long getId() {
		return this.id;
	}

	public City(String city, String district) {
		super();
		this.city = city;
		if (district == null)
			throw new RuntimeException("district to NULL");
		if (district.trim().equals(""))
			this.district = "- (brak) czy inne oznaczenie?";
		else {
			this.district = district;
		}
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}



}
