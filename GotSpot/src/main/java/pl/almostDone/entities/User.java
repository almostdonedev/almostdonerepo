package pl.almostDone.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "Users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Long id;
	@Column(unique = true)
	private String username;
	private String password;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="city_id")
	private City city;
	private Double rating;
	private Integer spots;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public Integer getSpots() {
		return spots;
	}
	public void setSpots(Integer spots) {
		this.spots = spots;
	}
	public Long getId() {
		return id;
	}
	public Double getRating() {
		return rating;
	}
	public User(String username, String password, City city) {
		super();
		this.username = username;
		this.password = password;
		this.city = city;
	}
	
	
}
