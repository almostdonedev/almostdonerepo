package pl.almostDone.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.jpa.criteria.predicate.NegatedPredicateWrapper;
import org.hibernate.metamodel.domain.Superclass;
import org.omg.CORBA.PRIVATE_MEMBER;

import pl.almostDone.DAO.Interfaces.CityDAO;
import pl.almostDone.DAO.Interfaces.UserDAO;
import pl.almostDone.DAO.impl.CityDAOImpl;
import pl.almostDone.DAO.impl.UserDAOImpl;
import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.entities.City;
import pl.almostDone.entities.User;

public class main {

	public static void main(String[] args) throws CityExistsInDBException {
		// TODO Auto-generated method stub

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("GotSpotPUTest");
		EntityManager em = entityManagerFactory.createEntityManager();
		
		City city1 = createCity("Sierpc", "brak");
		City city2 = createCity("Mochowo", "brak");
		
		User user1 = createUser("Huba", "test", city1);
		User user2 = createUser("Huba", "2test", city2);
		
		UserDAOImpl daoImpl  = new UserDAOImpl();
		daoImpl.setEntityManager(em);
		
		daoImpl.save(user1);
		daoImpl.save(user2);
		
		em.close();
		entityManagerFactory.close();
			
	}
	
	private static User createUser(String username, String password, City city) {
		User user = new User(username, password, city);
		return user;
		
	}
	
	private static City createCity(String cityName, String district) {
		City city = new City(cityName, district);
		return city;
	}

}