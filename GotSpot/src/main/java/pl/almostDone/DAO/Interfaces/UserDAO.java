package pl.almostDone.DAO.Interfaces;

import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.Exceptions.UserExistsInDBException;
import pl.almostDone.entities.User;

public interface UserDAO {

	public User addUser(User user) throws CityExistsInDBException, UserExistsInDBException;
	public Long userExist(User user);
}
