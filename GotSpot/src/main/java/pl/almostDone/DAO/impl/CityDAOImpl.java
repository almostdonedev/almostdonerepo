package pl.almostDone.DAO.impl;

import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.swing.text.StyledEditorKit.ForegroundAction;

import pl.almostDone.DAO.Interfaces.CityDAO;
import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.GenericDAO.GenericDAOImpl;
import pl.almostDone.entities.City;

public class CityDAOImpl extends GenericDAOImpl<City> implements CityDAO {

	public CityDAOImpl() {
		super(City.class);
	}
	
	public CityDAOImpl(EntityManager em) {
		super(City.class);
		entityManager = em;
	}

	public City addCity(City addingCity) throws CityExistsInDBException {
		City cityByName = getCityByName(addingCity);
		if (cityByName == null) {
			save(addingCity);
			return addingCity;
		}
		else {
			throw new CityExistsInDBException("Takie miasto już istnieje", cityByName);
		}

	}

	// Jeśli istnieje zwraca id, jeśli nie to null
	public City getCityByName(City city) {
		TypedQuery<City> query = entityManager.createQuery(
				"SELECT c FROM City c WHERE c.city = :cityName AND" + " c.district = :district", City.class);
		query.setParameter("cityName", city.getCity());
		query.setParameter("district", city.getDistrict());
		City found;
		try {
			found = query.getSingleResult();
			return found;
		} catch (NoResultException e) {
			return null;
		}

	}
	
	public Long getCitiesCount() {
		return getCount();
	}

	public List<City> getListmatchedCities(String substring) {
		
		System.out.println("To ten bład");
		String query = "SELECT c FROM City c WHERE c.city LIKE :substring  ";
		TypedQuery<City> typedQuery = entityManager.createQuery(query, City.class);
		typedQuery.setParameter("substring", substring + "%");
		
		List<City> resultList = typedQuery.getResultList();
		
		for (City city : resultList) {
			System.out.println(city.getCity());
		}
		return resultList;
	}

}
