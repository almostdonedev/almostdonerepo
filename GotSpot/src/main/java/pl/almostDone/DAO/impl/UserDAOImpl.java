package pl.almostDone.DAO.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.postgresql.translation.messages_bg;

import pl.almostDone.DAO.Interfaces.CityDAO;
import pl.almostDone.DAO.Interfaces.UserDAO;
import pl.almostDone.Exceptions.CityExistsInDBException;
import pl.almostDone.Exceptions.UserExistsInDBException;
import pl.almostDone.Exceptions.UserNotExistInDB;
import pl.almostDone.Exceptions.UserWrongPassword;
import pl.almostDone.GenericDAO.GenericDAOImpl;
import pl.almostDone.entities.City;
import pl.almostDone.entities.User;

public class UserDAOImpl extends GenericDAOImpl<User> implements UserDAO {

	CityDAO cityDAO;

	public UserDAOImpl() {
		super(User.class);
	}

	public User addUser(User user) throws UserExistsInDBException {
		
		if (userExist(user) != null ) {
			throw new UserExistsInDBException("Taki użytkownik już istnieje", user);
		}

		City userCity = user.getCity();

		cityDAO = new CityDAOImpl(this.entityManager);
		
		try {
			cityDAO.addCity(userCity);
		} catch (CityExistsInDBException e) {
			user.setCity(e.getCity());
		}
		save(user);
		return user;
	}

	public User getUserByUsername(String username) {
		TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.username = :username",
				User.class);
		query.setParameter("username", username);
		User found;
		try {
			found = query.getSingleResult();
			return found;
		} catch (NoResultException e) {
			return null;
		}
	}

	public Long userExist(User user) {
		User userByUsername = getUserByUsername(user.getUsername());
		if (userByUsername == null) {
			return null;
		} else {
			return userByUsername.getId();
		}
	}
	
	public Long getUsersCount() {
		return getCount();
	}

	public User checkLoginData(String username, String password) throws UserNotExistInDB, UserWrongPassword {
		User loggingUser = getUserByUsername(username);
		
		if (loggingUser == null) {
			throw new UserNotExistInDB(username, "Taki uzytkownik juz istnieje");
		}
		
		else if (!password.equals(loggingUser.getPassword())) {
			loggingUser = null;
			throw new UserWrongPassword(loggingUser, "Podane hasło jest nieprawidłowe");
		}
		else if (password.equals(loggingUser.getPassword())) {
			return loggingUser;
		}
		else {
			System.out.println("Nieobsłużony wyjątek");
			throw new RuntimeException();
		}
		
	}
	
	
}
